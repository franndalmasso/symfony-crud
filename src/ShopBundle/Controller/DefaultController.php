<?php

namespace ShopBundle\Controller;

use ShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="shop_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('ShopBundle:Product')->findAll();

        return $this->render('ShopBundle:Default:index.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="shop_product")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
        return $this->render('ShopBundle:Default:product.html.twig', array(
            'product' => $product
        ));
    }
}
