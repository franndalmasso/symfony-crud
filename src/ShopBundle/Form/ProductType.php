<?php

namespace ShopBundle\Form;

use ShopBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array( 
                'attr' => array('class' => 'form-control')
            ))
            ->add('image', TextType::class, array( 
                'attr' => array('class' => 'form-control')
            ))
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => function ($category) {
                    return $category->getName();
                },
                'attr' => array('class' => 'form-control')
            ))
            ->add('save', SubmitType::class,  array( 
                'attr' => array('class' => 'btn btn-success')
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ShopBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shopbundle_product';
    }


}
