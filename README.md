# Ar Webs Symfony

### Demo: [https://franndalmasso.000webhostapp.com/shop/](https://franndalmasso.000webhostapp.com/shop/)
**User:** admin
**Pass:** admin


**Requisitos necesarios:**

* Symfony 3.2 (MVC - POO) (https://symfony.com/doc/3.2)
* GIT (Bitbucket o Github)
* Single Page Application


**Tareas a realizar:**
* CRUD de Productos y Categorías (Acceso privado)
* CRUD de Usuarios (Acceso privado)
* Buscador de Productos (Acceso público)


**Entidades:**

Product
* name (string, not null)
* category (Category, not null)
* image (string): guarda la ruta de la imagen

Category
* name (string, not null)
* icon (string): guarda la ruta de la imagen

User
* username (string, not null)
* password (blob, not null)
* email (string, not null)